from dotenv import load_dotenv, find_dotenv
from flask_mongoengine import MongoEngine
import os
from datetime import datetime

db = MongoEngine()

# ----------------------------------------------------------------------------#
# setup_db(app)
#     binds a flask application and a MongoEngine service
# ----------------------------------------------------------------------------#
def setup_db(app):
	db.init_app(app)

def db_drop_and_create_all():
	db.drop_all()
	db.create_all()

# Users Model
'''
The Users Model columns are:
	> *Primary key*: _id <OBJECTID>
	- email <STRING> [Limit 255]
	- password <STRING> [Limit 255]
	- created_at <DATETIME> 
	- updated_at <DATETIME> 
'''
class Users(db.DynamicDocument):
	_id 	   = db.ObjectIdField(primary_key=True)
	email 	   = db.StringField(required=True, max_length=255)
	password   = db.StringField(required=True, max_length=191)
	created_at = db.DateTimeField(default=datetime.now)
	updated_at = db.DateTimeField(default=datetime.now)

	def __init__(self, **kwargs):
		super(Users, self).__init__(**kwargs)
		try:
			self.email = kwargs['email']
			self.password = kwargs['password']
			self.created_at = kwargs['created_at']
			self.updated_at = kwargs['updated_at']
		except:
			pass

	def info(self):
		return {
			'_id': str(self.id),
			'email': self.email,
			'password': self.password,
			'created_at': self.created_at,
			'updated_at': self.updated_at,
		}

# Weekly_Menu Model
'''
The Weekly_Menu Model columns are:
	> *Primary key*: _id <OBJECTID>
	- name <STRING> [Limit 255]
	- activates_at <DATETIME>
	- expires_at <DATETIME> 
	- created_at <DATETIME> 
	- updated_at <DATETIME> 
'''
class WeeklyMenu(db.DynamicDocument):
	_id 	     = db.ObjectIdField(primary_key=True)
	name    	 = db.StringField(required=True, max_length=255)
	activates_at = db.DateTimeField(default=datetime.now)
	expires_at   = db.DateTimeField(default=datetime.now)
	created_at   = db.DateTimeField(default=datetime.now)
	updated_at   = db.DateTimeField(default=datetime.now)

	def __init__(self, **kwargs):
		super(WeeklyMenu, self).__init__(**kwargs)
		try:
			self.name = kwargs['name']
			self.activates_at = kwargs['activates_at']
			self.expires_at = kwargs['expires_at']
			self.created_at = kwargs['created_at']
			self.updated_at = kwargs['updated_at']
		except:
			pass

	def info(self):
		return {
			'_id': str(self._id),
			'name': self.name,
			'activates_at': self.activates_at,
			'expires_at': self.expires_at,
			'created_at': self.created_at,
			'updated_at': self.updated_at,
		}

# Recipe Model
'''
The Recipe Model columns are:
	> *Primary key*: _id <OBJECTID>
	- menu_id <MENUS>
	- name <STRING> [Limit 255]
	- slug <STRING>  [Limit 255]
	- imagePath <STRING> 
	- description <STRING> 
	- prepTime <STRING> [Limit 255]
	- difficulty <INTEGER> [Limit 2]
	- created_at <DATETIME> 
	- updated_at <DATETIME> 
'''
class Recipes(db.DynamicDocument):
	_id 	     = db.ObjectIdField(primary_key=True)
	menu_id      = db.ReferenceField(WeeklyMenu, optional=True)
	name         = db.StringField(required=True, max_length=255)
	slug         = db.StringField(max_length=255)
	imagePath    = db.StringField(required=True)
	description  = db.StringField(required=True)
	prepTime     = db.StringField(required=True, max_length=255)
	difficulty   = db.IntField(required=True, max_length=2)
	created_at   = db.DateTimeField(default=datetime.now)
	updated_at   = db.DateTimeField(default=datetime.now)

	def __init__(self, **kwargs):
		super(Recipes, self).__init__(**kwargs)
		try:
			self._id = kwargs['_id']
			self.menu_id = kwargs['menu_id']
			self.name = kwargs['name']
			self.slug = kwargs['slug']
			self.imagePath = kwargs['imagePath']
			self.description = kwargs['description']
			self.prepTime = kwargs['prepTime']
			self.difficulty = kwargs['difficulty']
			self.created_at = kwargs['created_at']
			self.updated_at = kwargs['updated_at']
		except:
			pass

	def info(self):
		return {
			'_id': str(self._id),
			'menu_id' : str(self.menu_id),
			'name': self.name,
			'slug': self.slug,
			'imagePath': self.imagePath,
			'description': self.description,
			'prepTime': self.prepTime,
			'difficulty': self.difficulty,
			'created_at': self.created_at,
			'updated_at': self.updated_at,
		}

# Ingredients Model
'''
The Ingredients Model columns are:
	> *Primary key*: _id <OBJECTID>
	- name <STRING> [Limit 255]
	- slug <STRING>  [Limit 255]
	- imagePath <STRING> 
	- created_at <DATETIME> 
	- updated_at <DATETIME> 
'''
class Ingredients(db.DynamicDocument):
	_id 	     = db.ObjectIdField(primary_key=True)
	name         = db.StringField(required=True, max_length=255)
	slug         = db.StringField(required=True, max_length=255)
	imagePath    = db.StringField(required=True)
	created_at   = db.DateTimeField(default=datetime.now)
	updated_at   = db.DateTimeField(default=datetime.now)

	def __init__(self, **kwargs):
		super(Ingredients, self).__init__(**kwargs)
		try:
			self.name = kwargs['name']
			self.slug = kwargs['slug']
			self.imagePath = kwargs['imagePath']
			self.created_at = kwargs['created_at']
			self.updated_at = kwargs['updated_at']
		except:
			pass

	def info(self):
		return {
			'_id': str(self._id),
			'name': self.name,
			'slug': self.slug,
			'imagePath': self.imagePath,
			'created_at': self.created_at,
			'updated_at': self.updated_at,
		}

# MeasurementQty Model
'''
The MeasurementQty Model columns are:
	> *Primary key*: _id <OBJECTID>
	- qty_amount <STRING> [Limit 255]
	- created_at <DATETIME> 
	- updated_at <DATETIME> 
'''
class MeasurementQty(db.DynamicDocument):
	_id 	     = db.ObjectIdField(primary_key=True)
	qty_amount   = db.StringField(required=True, max_length=255)
	created_at   = db.DateTimeField(default=datetime.now)
	updated_at   = db.DateTimeField(default=datetime.now)

	def __init__(self, **kwargs):
		super(MeasurementQty, self).__init__(**kwargs)
		try:
			self.qty_amount = kwargs['qty_amount']
			self.created_at = kwargs['created_at']
			self.updated_at = kwargs['updated_at']
		except:
			pass

	def info(self):
		return {
			'_id': str(self._id),
			'qty_amount': self.qty_amount,
			'created_at': self.created_at,
			'updated_at': self.updated_at,
		}

# MeasurementUnits Model
'''
The MeasurementUnits Model columns are:
	> *Primary key*: _id <OBJECTID>
	- qty_amount <STRING> [Limit 255]
	- created_at <DATETIME> 
	- updated_at <DATETIME> 
'''
class MeasurementUnits(db.DynamicDocument):
	_id 	     = db.ObjectIdField(primary_key=True)
	description  = db.StringField(required=True, max_length=255)
	created_at   = db.DateTimeField(default=datetime.now)
	updated_at   = db.DateTimeField(default=datetime.now)

	def __init__(self, **kwargs):
		super(MeasurementUnits, self).__init__(**kwargs)
		try:
			self.description = kwargs['description']
			self.created_at = kwargs['created_at']
			self.updated_at = kwargs['updated_at']
		except:
			pass

	def info(self):
		return {
			'_id': str(self._id),
			'description': self.description,
			'created_at': self.created_at,
			'updated_at': self.updated_at,
		}

# RecipeIngredients Model
'''
The RecipeIngredients Model columns are:
	> *Primary key*: _id <OBJECTID>
	- recipes_id <RECIPES>
	- mreasurement_id <MEASUREMENTUNITS>
	- measurement_qty_id <MEASUUREMENTQTY>
	- created_at <DATETIME> 
	- updated_at <DATETIME> 
'''
class RecipeIngredients(db.DynamicDocument):
	_id 	            = db.ObjectIdField(primary_key=True)
	recipes_id          = db.ReferenceField(Recipes)
	measurement_id      = db.ReferenceField(MeasurementUnits)
	measurement_qty_id  = db.ReferenceField(MeasurementQty)
	ingredient_id		= db.ReferenceField(Ingredients)
	created_at   		= db.DateTimeField(default=datetime.now)
	updated_at   		= db.DateTimeField(default=datetime.now)

	def __init__(self, **kwargs):
		super(RecipeIngredients, self).__init__(**kwargs)
		try:
			self.recipes_id = kwargs['recipes_id']
			self.measurement_id = kwargs['measurement_id']
			self.measurement_qty_id = kwargs['measurement_qty_id']
			self.ingredient_id = kwargs['ingredient_id']
			self.created_at = kwargs['created_at']
			self.updated_at = kwargs['updated_at']
		except:
			pass

	def info(self):
		return {
			'_id': str(self._id),
			'recipes_id' : str(self.recipes_id),
			'measurement_id': str(self.measurement_id),
			'measurement_qty_id': str(self.measurement_qty_id),
			'ingredient_id': str(self.ingredient_id),
			'created_at': self.created_at,
			'updated_at': self.updated_at,
		}

# Utensils Model
'''
The Utensils Model columns are:
	> *Primary key*: _id <OBJECTID>
	- name <STRING> [Limit 255]
	- slug <STRING> [Limit 255]
	- created_at <DATETIME> 
	- updated_at <DATETIME> 
'''
class Utensils(db.DynamicDocument):
	_id 	     = db.ObjectIdField(primary_key=True)
	name   		 = db.StringField(required=True, max_length=255)
	slug   		 = db.StringField(required=True, max_length=255)
	created_at   = db.DateTimeField(default=datetime.now)
	updated_at   = db.DateTimeField(default=datetime.now)

	def __init__(self, **kwargs):
		super(Utensils, self).__init__(**kwargs)
		try:
			self.name = kwargs['name']
			self.slug = kwargs['slug']
			self.created_at = kwargs['created_at']
			self.updated_at = kwargs['updated_at']
		except:
			pass

	def info(self):
		return {
			'_id': str(self._id),
			'name': self.name,
			'slug': self.slug,
			'created_at': self.created_at,
			'updated_at': self.updated_at,
		}

# RecipeUtensils Model
'''
The RecipeUtensils Model columns are:
	> *Primary key*: _id <STRING>
	- recipes_id <RECIPES>
	- mreasurement_id <STRING> [Limit 255]
	- measurement_qty_id <STRING>  [Limit 255]
	- created_at <DATETIME> 
	- updated_at <DATETIME> 
'''
class RecipeUtensils(db.DynamicDocument):
	_id 	            = db.ObjectIdField(primary_key=True)
	recipes_id          = db.ReferenceField(Recipes)
	utensils_id			= db.ReferenceField(Utensils)
	created_at   		= db.DateTimeField(default=datetime.now)
	updated_at   		= db.DateTimeField(default=datetime.now)

	def __init__(self, **kwargs):
		super(RecipeUtensils, self).__init__(**kwargs)
		try:
			self.recipes_id = kwargs['recipes_id']
			self.utensils_id = kwargs['utensils_id']
			self.created_at = kwargs['created_at']
			self.updated_at = kwargs['updated_at']
		except:
			pass

	def info(self):
		return {
			'_id': str(self._id),
			'recipes_id' : str(self.recipes_id),
			'utensils_id': str(self.utensils_id),
			'created_at': self.created_at,
			'updated_at': self.updated_at,
		}


# Steps Model
'''
The Steps Model columns are:
	> *Primary key*: _id <STRING>
	- recipes_id <RECIPES>
	- instructions <STRING>
	- sort_order <INTEGER> [Limit 10]
	- created_at <DATETIME> 
	- updated_at <DATETIME> 
'''
class Steps(db.DynamicDocument):
	_id 	            = db.ObjectIdField(primary_key=True)
	recipes_id          = db.ReferenceField(Recipes)
	instructions		= db.StringField(required=True)
	sort_order			= db.IntField(required=True, max_length=10)
	timers				= db.ListField(db.StringField())
	created_at   		= db.DateTimeField(default=datetime.now)
	updated_at   		= db.DateTimeField(default=datetime.now)

	def __init__(self, **kwargs):
		super(Steps, self).__init__(**kwargs)
		try:
			self.recipes_id = kwargs['recipes_id']
			self.instructions = kwargs['instructions']
			self.sort_order = kwargs['sort_order']
			self.timers = kwarsg['timers']
			self.created_at = kwargs['created_at']
			self.updated_at = kwargs['updated_at']
		except:
			pass

	def info(self):
		return {
			'_id': str(self._id),
			'recipes_id' : str(self.recipes_id),
			'instructions': self.instructions,
			'sort_order': self.sort_order,
			'timers' : self.timers,
			'created_at': self.created_at,
			'updated_at': self.updated_at,
		}

# StepsMedia Model
'''
The StepsMedia Model columns are:
	> *Primary key*: _id <STRING>
	- steps_id <STEPS>
	- link <STRING>
	- caption <STRING> [Limit 255]
	- created_at <DATETIME> 
	- updated_at <DATETIME> 
'''
class StepsMedia(db.DynamicDocument):
	_id 	            = db.ObjectIdField(primary_key=True)
	steps_id          	= db.ReferenceField(Steps)
	link				= db.StringField(required=True)
	caption				= db.StringField(required=True, max_length=255)
	created_at   		= db.DateTimeField(default=datetime.now)
	updated_at   		= db.DateTimeField(default=datetime.now)

	def __init__(self, **kwargs):
		super(StepsMedia, self).__init__(**kwargs)
		try:
			self.steps_id = kwargs['steps_id']
			self.link = kwargs['link']
			self.caption = kwargs['caption']
			self.created_at = kwargs['created_at']
			self.updated_at = kwargs['updated_at']
		except:
			pass

	def info(self):
		return {
			'_id': str(self._id),
			'steps_id' : str(self.steps_id),
			'link': self.link,
			'caption': self.caption,
			'created_at': self.created_at,
			'updated_at': self.updated_at,
		}