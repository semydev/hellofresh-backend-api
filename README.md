# HelloFresh backend API

This was a takehome assignment to be completed as part of the technical interview for a software development role.
Assignment description can be found here: https://github.com/hello-abhishek/hf-take-home-programming-challenges/blob/main/SOFTWARE-ENGINEER.md

This project is a simple CRUD API revolving around weekly menu's, recipes, ingredients, steps and so forth. Given the really tight time constraints I have decided to leave out various info like nutritional information as well as a few other CRUD endpoints liek creating/updating/deleting of ingredients and utensils.

I have used a Stack I am least familiar with as more of a challenge to myself and take it as a learning curve. For me, developing this in a Node.js+Mongo stack would have been alot easier. I also chose to do the back-end task as opposed to the much easier front-end task and for this reason I hope doesnt hinder my chance of progressing to the next step, otherwise please do tell me and I would like to partake in the front-end task which is miles easier due to not having to develop an entire database and relational modal and instead just having to pull data from an API which take sa couple of seconds. 

My next steps would include finishing the aforementioned endpoints then beging componetising things a lot more and divide them into seperate classes. The main app.py is very cludded and is largely a result of only having 2 nights to finish the assignment in which totals about 8 hours of development time. From there I would develop a plethora of test cases, them in themselves takes multiple hours to be implemented so unsure how this is all to be factored into tight deadline.

Please see postman collection for working with the API. Can run the script with python app.py else you can use docker

