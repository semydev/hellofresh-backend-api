import pymongo, logging, json, os, bcrypt
from slugify import slugify
from bson import ObjectId
from datetime import datetime
from flask import Flask, request, render_template, redirect, make_response, jsonify
from flask_jwt_extended import create_access_token, get_jwt_identity, jwt_required, JWTManager
from flask_mongoengine import MongoEngine
from flask_cors import CORS
from dotenv import load_dotenv
load_dotenv()
from models import db_drop_and_create_all, setup_db, db, Users, WeeklyMenu, Recipes, RecipeIngredients, Ingredients, MeasurementQty, MeasurementUnits, RecipeUtensils, Utensils, Steps, StepsMedia

app = Flask(__name__)
app.config["SECRET_KEY"] = os.getenv('SECRET_KEY')
app.config['MONGODB_SETTINGS'] = {
	'host': os.getenv('MONGO_URI'),
	'connect': False,
}

# Setup DB
db = MongoEngine()
db.init_app(app)
setup_db(app)

# Initializes CORS so that the api_tool can talk to the example app
CORS(app)
jwt = JWTManager(app)

#Set up some routes for the API
@app.route('/api/', methods=['GET'])
def home():
	return {"message": "HelloFresh Backend API take home assignment", "success": True}, 200


# Auth Routes
@app.route('/api/signup', methods=['POST'])
def singup():
	"""
	Signs a user up
	"""
	req = request.get_json(force=True)
	_email = req.get('email', None)
	_password = req.get('password', None)

	# validate the received values
	if _email and _password:
		_hashed_password = bcrypt.hashpw(_password.encode('utf-8'), bcrypt.gensalt()).decode("utf-8")
		users = Users.objects(email=_email).first()
		if not users:
			usersave = Users(_id=ObjectId(), email=_email, password=_hashed_password, created_at=datetime.now(), updated_at=datetime.now())
			usersave.save()
			return make_response(jsonify({ "message": "Registration Successful", "success": True }), 200)
		else:
			return make_response(jsonify({ "message": "A user with this email already exists", "success": False }), 200)
	else:
		return make_response(jsonify({ "message": "Missing required fields for signup", "success" : False }), 401)

   
@app.route('/api/login', methods=['POST'])
def login():
	"""
	Logs a user in by parsing a POST request containing user credentials and
	issuing a JWT token.
	"""
	req = request.get_json(force=True)
	_email = req.get('email', None)
	_password = req.get('password', None)

	# validate the received values
	if _email and _password:
		login_user = Users.objects(email=_email).first()
		if login_user:
			if bcrypt.hashpw(_password.encode('utf-8'), login_user['password'].encode('utf-8')) == login_user['password'].encode('utf-8'):
				access_token = create_access_token(identity=login_user['email'])
				return make_response(jsonify({ "access_token": access_token, "success": True }), 200)
			else:
				return make_response(jsonify({ "message": "Invalid Email/Password combination", "success" : False }), 401)
		else:
			return make_response(jsonify({ "message": "No User Account Found", "success" : False }), 401)
	else:
		return make_response(jsonify({ "message": "Invalid Email/Password combination", "success" : False }), 401)


# Menu routes
@app.route('/api/menus', methods=["GET"])
@jwt_required()
def get_menus():
	"""
	Menus will return a list of all menus
	"""
	menu_list = []
	_menus = WeeklyMenu.objects()
	menu_list = [menu.info() for menu in _menus]
	return make_response(jsonify({ "menus": menu_list, "success": True }), 200)
	

@app.route('/api/menus/<menu_id>', methods=["GET"])
@jwt_required()
def get_menu(menu_id):
	"""
	Get Menu will return a single menu with recipes that matches the provided menu ID
	"""
	try:
		menu_objectId = ObjectId(menu_id)
	except:
		return make_response(jsonify({ "message" : "No menu found with ID: %s" % menu_id, "success" : False }), 404)

	_menu = WeeklyMenu.objects(_id=menu_objectId).first()
	if _menu == None:
		return make_response(jsonify({ "message" : "No menu found with ID: %s" % menu_id, "success" : False }), 404)

	_recipes = Recipes.objects(menu_id=menu_objectId)
	recipes_list = [recipe.info() for recipe in _recipes]

	responseBody = {
		'menu_id' : str(_menu._id),
		'start_date' : _menu.activates_at,
		'end_date' : _menu.expires_at,
		'recipes' : recipes_list,
		'success': True
	}
	return make_response(jsonify(responseBody), 200)


@app.route('/api/menus', methods=["POST"])
@jwt_required()
def create_menu():
	"""
	Will create a new menu with given name, activates_at date and expires_at date
	"""
	req = request.get_json(force=True)
	_name = req.get('name', None)
	_activates_at = req.get('activates_at', None)
	_expires_at = req.get('activates_at', None)

	# validate the received values
	if _name and _activates_at and _expires_at:
		existing_menu = WeeklyMenu.objects(name=_name).first()
		if not existing_menu:
			new_menu = WeeklyMenu(name=_name, activates_at=_activates_at, expires_at=_expires_at)
			new_menu.save()
			return make_response(jsonify({ "message": "New Menu Successfully Created", "success": True }), 200)
		else:
			return make_response(jsonify({ "message": "An existing menu was found with that name", "success" : False }), 401)
	else:
		return make_response(jsonify({ "message": "Missing required fields to create a menu", "success" : False }), 401)


@app.route('/api/menus/<menu_id>', methods=["POST"])
@jwt_required()
def update_menu(menu_id):
	"""
	Will update menu of given ID with the data given in the payload
	"""
	try:
		menu_objectId = ObjectId(menu_id)
	except:
		return make_response(jsonify({ "message" : "No menu found with ID: %s" % menu_id, "success" : False }), 404)

	req = request.get_json(force=True)
	_name = req.get('name', None)
	_activates_at = req.get('activates_at', None)
	_expires_at = req.get('activates_at', None)

	# validate the received values
	if _name and _activates_at and _expires_at:
		existing_menu = WeeklyMenu.objects(_id=menu_objectId).first()
		if existing_menu:
			existing_menu.update(
				name=_name,
				activates_at=_activates_at,
				expires_at=_expires_at
			)
			existing_menu.save()
			return make_response(jsonify({ "message": "Successfully updated menu with ID: %s" % menu_id, "success": True }), 200)
		else:
			return make_response(jsonify({ "message": "No menu was found with ID: %s" % menu_id, "success" : False }), 401)
	else:
		return make_response(jsonify({ "message": "Missing required fields to update Menu", "success" : False }), 401)

	responseBody = { "message": "Update Menu with ID: %s" % menu_id, "success": True }
	return make_response(jsonify(responseBody), 200)


@app.route('/api/menus/<menu_id>', methods=["DELETE"])
@jwt_required()
def delete_menu(menu_id):
	"""
	Will delete only the menu of supplied ID
	"""
	try:
		menu_objectId = ObjectId(menu_id)
		existing_menu = WeeklyMenu.objects.get(_id=menu_objectId)
		if existing_menu:
			existing_menu.delete()
			return make_response(jsonify({ "message": "Successfully deleted menu with ID: %s" % menu_id, "success": True }), 200)
	except Exception as e:
		return make_response(jsonify({ "message": "Failed to delete menu with ID %s" % menu_id, "success": False }), 401)


# Utensils Routes
@app.route('/api/utensils', methods=["GET"])
@jwt_required()
def get_utensils():
	"""
	Utensils will return a list of all utensils in the database
	"""
	_utensils = Utensils.objects()
	utensils_list = [utensil.info() for utensil in _utensils]
	return make_response(jsonify({ "utensils": utensils_list, "success": True }), 200)


# Ingredient Routes
@app.route('/api/ingredients', methods=["GET"])
@jwt_required()
def get_ingredients():
	"""
	Ingredients will return a list of all ingredients in the database
	"""
	_ingredients = Ingredients.objects()
	ingredient_list = [ingredient.info() for ingredient in _ingredients]
	return make_response(jsonify({ "ingredients": ingredient_list, "success": True }), 200)


# Recipe Routes
@app.route('/api/recipes', methods=["GET"])
@jwt_required()
def get_recipes():
	"""
	Recipes will return a list of all recipes in the databse
	"""
	_recipes = Recipes.objects()
	recipes_list = [recipe.info() for recipe in _recipes]
	return make_response(jsonify({ "recipes": recipes_list, "success": True }), 200)


@app.route('/api/recipes/<recipe_id>', methods=["GET"])
@jwt_required()
def get_recipe(recipe_id):
	"""
	Get Recipe will return a single recipe that matches the provided recipe ID
	This function also find relational data such as ingredients, utencils and steps for that recipe
	and returns these all together in a single payload
	"""
	ingredients_list = []
	utencils_list = []
	recipe_steps = []

	# Check if valid recipe ID first by determien whether it can be converted into a ObjectID
	try:
		recipe_objectId = ObjectId(recipe_id)
	except:
		return make_response(jsonify({ "message" : "No recipe found with ID: %s" % recipe_id, "success" : False }), 404)

	# Fecth recipe and recipe_ingredients from mapping table before looping over each to find measure qty and units
	recipe = Recipes.objects(_id=recipe_objectId).first().info()
	_recipe_ingredients = RecipeIngredients.objects(recipes_id=recipe_objectId)
	for recipe_ingredient in _recipe_ingredients:
		ingredient_dict = recipe_ingredient.ingredient_id.info()
		ingredient_dict['qty_amount'] = None
		ingredient_dict['measurement_units'] = None

		if recipe_ingredient['measurement_id'] is not None:
			ingredient_dict['measurement_units'] = recipe_ingredient.measurement_id.description

		if recipe_ingredient['measurement_qty_id'] is not None:
			ingredient_dict['qty_amount'] = recipe_ingredient.measurement_qty_id.qty_amount

		ingredients_list.append(ingredient_dict)

	# Check mapping table to find required utensils for this recipe
	_recipe_utensils = RecipeUtensils.objects(recipes_id=recipe_objectId)
	utencils_list = [recipe_utensil.utensils_id.info() for recipe_utensil in _recipe_utensils]

	# Find steps associated with this recipe
	_steps = Steps.objects(recipes_id=recipe_objectId)
	for step in _steps:
		# For Each step - find any associated media images to help explain the stop visually
		_steps_media = StepsMedia.objects(steps_id=step.info()['_id']).first()
		formatted_steps = {
			'ingredients': [],
			'instructionsHTML' : step.instructions,
			'utensils': [],
			'timers' : [],
			'index': step.sort_order,
			'images': [_steps_media.info()]
		}
		recipe_steps.append(formatted_steps)

	responseBody = {'success': True, **recipe, 'ingredients': ingredients_list, 'steps': recipe_steps, 'utencils': utencils_list}
	return make_response(jsonify(responseBody), 200)


@app.route('/api/recipes', methods=["POST"])
@jwt_required()
def create_recipe():
	"""
	Will create a recipe
	"""
	req = request.get_json(force=True)

	# Extract data from payload to be used to create new recipe
	_name = req.get('name', None)
	_description = req.get('description', None)
	_difficulty = req.get('difficulty', None)
	_imagePath = req.get('imagePath', None)
	_prepTime = req.get('prepTime', None)
	_ingredients = req.get('ingredients', None)
	_steps = req.get('steps', None)
	_utencils = req.get('utencils', None)

	# validate the received required values
	if not _name and not _description and not _imagePath and not _prepTime and not _difficulty:
		return make_response(jsonify({ "message": "Recipe is missing required fields", "success" : False }), 401)

	existing_recipe = Recipes.objects(name=_name).first()
	if existing_recipe:
		return make_response(jsonify({ "message": "An existing recipe was found with that name", "success" : False }), 401)

	# Create new Recipe
	_slug = slugify(_name)
	new_recipe = Recipes(
		_id=ObjectId(),
		name=_name, 
		slug=_slug, 
		menu_id=None,
		description=_description, 
		imagePath=_imagePath,
		prepTime=_prepTime,
		difficulty=_difficulty
	)
	new_recipe.save()

	# Assign Ingredients with new Recipe in recipe_ingredients table
	# Given measurement units and quantity - find their respective ID's to be used for insertion to the
	# mapping table
	if len(_ingredients) > 0:
		for ingredient in _ingredients:
			if ingredient['qty_amount'] == None:
				continue
			measurement_unit = MeasurementUnits.objects(description=ingredient['measurement_units']).first()
			measurement_qty_id = MeasurementQty.objects(qty_amount=ingredient['qty_amount']).first()
			recipe_ingredient = RecipeIngredients(
				_id=ObjectId(),
				recipes_id=new_recipe,
				measurement_id=measurement_unit,
				measurement_qty_id=measurement_qty_id,
				ingredient_id=ObjectId(ingredient['_id'])
			)
			recipe_ingredient.save()

	# Insert steps with the new recipe into the steps table
	if len(_steps) > 0:
		for step in _steps:
			# Create New Step 
			recipe_step = Steps(
				_id=ObjectId(),
				recipes_id=new_recipe,
				instructions=step['instructionsHTML'],
				sort_order=step['index'],
				timers=step['timers'],
				utensils=step['utensils']
			)
			recipe_step.save()

			# Create Image entry linked to previously created step
			for image in step['images']:
				step_media = StepsMedia(
					_id=ObjectId(),
					caption=image['caption'],
					link=image['link'],
					steps_id=recipe_step
				)
				step_media.save()
	return make_response(jsonify({ "message": "New Recipe Successfully Created", "success": True }), 200)


@app.route('/api/recipes/<recipe_id>', methods=["POST"])
@jwt_required()
def update_recipe(recipe_id):
	"""
	Will update recipe of given ID with the data given in the payload
	"""
	try:
		recipe_objectId = ObjectId(recipe_id)
	except:
		return make_response(jsonify({ "message" : "No recipe found with ID: %s" % recipe_id, "success" : False }), 404)

	# Extract data from payload to be used to create new recipe
	req = request.get_json(force=True)
	_name = req.get('name', None)
	_description = req.get('description', None)
	_difficulty = req.get('difficulty', None)
	_imagePath = req.get('imagePath', None)
	_prepTime = req.get('prepTime', None)
	_ingredients = req.get('ingredients', None)
	_steps = req.get('steps', None)
	_utencils = req.get('utencils', None)

	# validate the received required values
	if not _name and not _description and not _imagePath and not _prepTime and not _difficulty:
		return make_response(jsonify({ "message": "Recipe is missing required fields", "success" : False }), 401)

	existing_recipe = Recipes.objects(_id=recipe_objectId).first()
	if not existing_recipe:
		return make_response(jsonify({ "message": "No recipe found with ID: %s" % recipe_id, "success" : False }), 401)

	# Update all Recipe fields with those that have been provided
	existing_recipe.update(
		name=_name,
		slug=slugify(_name),
		description=_description,
		imagePath=_imagePath,
		prepTime=_prepTime,
		ingredients=_ingredients,
		steps=_steps,
		utencils=_utencils
	)

	# For each recipe_ingredient in the mapping table, check if it exists in update ingredients payload, if not
	# then delete it. Can be combined with the above loop hoever have split into two sperates for readibility and simplicity
	recipe_ingredients = RecipeIngredients.objects(recipes_id=existing_recipe._id)
	for recipe_ingredient in recipe_ingredients:
		exists = False
		# Check over supplied ingredients to determine if ingredient is still used in recipe
		for ingredient in _ingredients:
			if ObjectId(ingredient['_id']) == recipe_ingredient.ingredient_id._id:
				exists = True
		if exists == False:
			# Delete entry from mapping table as recipe no longer requires this ingredient
			recipe_ingredient.delete()


	# Taking the update payload as the source truth, remove all 
	for ingredient in _ingredients:
		# Find the mapping row for that recipe + ingredient combo
		recipe_ingredient = RecipeIngredients.objects(recipes_id=existing_recipe._id, ingredient_id=ObjectId(ingredient['_id'])).first()

		# If new ingredient for recipe and not in mapping table then insert it
		if not recipe_ingredient:
			measurement_unit = MeasurementUnits.objects(description=ingredient['measurement_units']).first()
			measurement_qty_id = MeasurementQty.objects(qty_amount=ingredient['qty_amount']).first()
			recipe_ingredient = RecipeIngredients(
				_id=ObjectId(),
				recipes_id=existing_recipe._id,
				measurement_id=measurement_unit,
				measurement_qty_id=measurement_qty_id,
				ingredient_id=ObjectId(ingredient['_id'])
			)
			recipe_ingredient.save()
			continue
		else:
			# Check if there is a specific measurement unit for the ingredient for the resume
			if ingredient['measurement_units'] != None:
				recipe_ingredient.update(measurement_id=MeasurementUnits.objects(description=ingredient['measurement_units']).first()._id)

			# Check if there is a specific quantity for the ingredient for the resume
			if ingredient['qty_amount'] != None:
				recipe_ingredient.update(measurement_qty_id=MeasurementQty.objects(qty_amount=str(ingredient['qty_amount'])).first()._id)
			continue

	responseBody = { "message": "Updated recipe with ID: %s" % recipe_id, "success": True }
	return make_response(jsonify(responseBody), 200)


@app.route('/api/recipes/<recipe_id>', methods=["DELETE"])
@jwt_required()
def delete_recipe(recipe_id):
	"""
	Will delete recipe of given ID
	After deleting this recipe, it will attempt to delete mapping refrences from the Recipe_Ingredients
	and Recipe_Utensils table that are associated with the su0pplied recipe_id
	"""
	try:
		existing_recipe = Recipes.objects(_id=ObjectId(recipe_id)).first()
		if existing_recipe:
			existing_recipe.delete()

			existing_recipe_ingredients = RecipeIngredients.objects(recipe_id=ObjectId(recipe_id)).first()
			if existing_recipe_ingredients:
				existing_recipe_ingredients.delete()

			existing_recipe_utensils = RecipeUtensils.objects(recipe_id=ObjectId(recipe_id)).first()
			if existing_recipe_utensils:
				existing_recipe_utensils.delete()

			return make_response(jsonify({ "message": "Successfully deleted recipe with ID: %s" % recipe_id, "success": True }), 200)
		return make_response(jsonify({ "message": "No recipe with ID %s to delete" % recipe_id, "success": True }), 200)
	except Exception as e:
		print(e)
		return make_response(jsonify({ "message": "Failed to delete recipe with ID %s" % recipe_id, "success": False }), 401)



if __name__ == "__main__":
	app.run(host='0.0.0.0', port=5000, debug=True)