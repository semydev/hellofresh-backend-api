FROM python:3.8

COPY requirements.txt ./requirements.txt
COPY models.py ./models.py
COPY app.py ./app.py
COPY test.py ./test.py

ENV MONGO_URI="mongodb+srv://admin2:Ab123456@cluster0.at7j9.mongodb.net/hellofresh?retryWrites=true&w=majority"
ENV SECRET_KEY="HFGu85GGj"

RUN pip install -r requirements.txt

CMD ["python", "./app.py"]