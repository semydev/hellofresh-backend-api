from app import app
import unittest, json

class HelloFreshTest(unittest.TestCase):

	# ----------------------------------------------------------------------------#
	# Used Test-Driven-Development in the making of this API
	# Minimum of 2 tests for each endpoint!
	# ----------------------------------------------------------------------------#
	
	# Check for response 200
	def test_home(self):
		tester = app.test_client(self)
		response = tester.get('/api/')
		self.assertEqual(response.status_code, 200)

	# Check if content return is application/json
	def test_home_content(self):
		tester = app.test_client(self)
		response = tester.get('/api/')
		self.assertEqual(response.content_type, "application/json")

	# Test /api/menus endpoint for all menus and specific menu
	def test_get_menus(self):
		"""Test for get_menus() GET /api/menus"""
		# Test the following:
		#   - Menus can be retrieved
		#   - Response is 200
		#   - Length of menus is not 0
		tester = app.test_client(self)
		response = tester.get('/api/menus')
		data = json.loads(response.data)
		self.assertEqual(response.status_code, 200)
		self.assertEqual(data['success'], True)
		self.assertTrue(len(data['menus']))

	def test_get_menu(self):
		"""Test for get_menu() GET /api/menus/60ffeef2c9fd44cd59d81029"""
		# Test the following:
		#   - Menu can be retrieved
		#   - menu_id is in payload
		#   - Response is 200
		tester = app.test_client(self)
		response = tester.get(f'/api/menus/60ffeef2c9fd44cd59d81029')
		data = json.loads(response.data)
		self.assertEqual(response.status_code, 200)
		self.assertEqual(data['success'], True)
		self.assertEqual(data['menu_id'], '60ffeef2c9fd44cd59d81029')

	def test_404_no_menu(self):
		"""Test get_menu() for non-existing menu - prompt error 404"""
		# Test the following:
		#   - Invalid menu id results in a respone of 404
		tester = app.test_client(self)
		response = tester.get(f'/api/menus/60ffeef2c9fd44cd59d81028')
		data = json.loads(response.data)
		self.assertEqual(response.status_code, 404)
		self.assertEqual(data['success'], False)
		self.assertEqual(data['message'], 'No menu found with ID: 60ffeef2c9fd44cd59d81028')


	# Test /api/ingredients endpoint for all ingredients
	def test_get_ingredients(self):
		"""Test for get_ingredients() GET /api/ingredients"""
		# Test the following:
		#   - Ingredients can be retrieved
		#   - Response is 200
		#   - Length of ingredients is not 0
		tester = app.test_client(self)
		response = tester.get('/api/ingredients')
		data = json.loads(response.data)
		self.assertEqual(response.status_code, 200)
		self.assertEqual(data['success'], True)
		self.assertTrue(len(data['ingredients']))


		# Test /api/utensils endpoint for all utensils
	def test_get_utensils(self):
		"""Test for get_utensils() GET /api/utensils"""
		# Test the following:
		#   - Utensils can be retrieved
		#   - Response is 200
		#   - Length of utensils is not 0
		tester = app.test_client(self)
		response = tester.get('/api/utensils')
		data = json.loads(response.data)
		self.assertEqual(response.status_code, 200)
		self.assertEqual(data['success'], True)
		self.assertTrue(len(data['utensils']))


# Make the tests conveniently executable
if __name__ == "__main__":
	unittest.main()